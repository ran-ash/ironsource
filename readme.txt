----- README -----
The repository contains 2 folders:

1. backend - NodeJS app listening on port 3000 run from "main.js".
			 Also contains a script to initialize the Mongo DB called "resetDb.js"
			 The app requires a MongoDB listening locally on localhost:27017

2. frontend - Angular 1.6.x website
			  The app currently makes API calls to "http://54.93.224.139:3000/"
			  This can be changed in /scripts/api.constants.js