(function () {
    angular.module("app", ["ngRoute"]);
})();

(function () {
    angular.module("app").config([
        "$routeProvider",
        function ($routeProvider) {

            $routeProvider
                .when("/", {
                    templateUrl: "test.html",
                    controller: "testController as tc"
                });
        }
    ]);
})();

(function () {
    angular.module("app").controller("testController", testController);
    testController.$inject = ["testService"];

    function testController(testService) {
        var vm = this;

        var x = 5;
    }
}());

(function () {
    angular.module("app").service("testService", testService);

    function testService() {

        
    }
})();