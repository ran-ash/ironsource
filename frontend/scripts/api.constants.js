(function () {
    angular.module("app").constant("apiConstants", {
        host: "http://54.93.224.139:3000/",
        urls: {
            getContacts: "api/getContacts",
			addContact: "api/addContact",
			deleteContact: "api/deleteContact"
        }
    });
})();