(function () {
    angular.module("app").controller("contactsController", contactsController);
    contactsController.$inject = ["$http", "apiConstants"];

    function contactsController($http, apiConstants) {
        var vm = this;
        var postConfig = { headers:  { "Content-Type" : "application/json" } };

        vm.filterObject = {
            searchString: '',
            sortColumn: 'firstName',
            sortAscending: true,
            pageNumber: 1,
            pageSize: 100
        };
        vm.pageSizeOptions = [25, 50, 75, 100];
        vm.pageNumberOptions = [];
        vm.contactsViewModel = {};
        vm.newContact = {};
        vm.addContact = addContact;
        vm.cancelAddNewContact = cancelAddNewContact;
        vm.deleteContact = deleteContact;
        vm.getContacts = getContacts;
        vm.sort = sort;
        vm.search = search;
        vm.clearSearch = clearSearch;

        function initNewContact() {
            vm.newContact.firstName = "";
            vm.newContact.lastName = "";
            vm.newContact.phone = "";
            vm.newContact.email = "";
            vm.newContact.address = "";
        }

        function search() {
            vm.filterObject.pageNumber = 1;
            vm.filterObject.pageSize = 100;

            getContacts();
        }

        function clearSearch(){
            if (vm.filterObject.searchString) {
                vm.filterObject.searchString='';
                getContacts();
            }
        }

        function cancelAddNewContact() {
            vm.showNewContact = false;
            initNewContact();
        }

        function sort(columnName) {
            if (columnName === vm.filterObject.sortColumn) {
                vm.filterObject.sortAscending = !vm.filterObject.sortAscending
            } else {
                vm.filterObject.sortColumn = columnName;
                vm.filterObject.sortAscending = true;
            }

            getContacts();
        }
        
        function addContact() {
            $http.post(apiConstants.host+apiConstants.urls.addContact, vm.newContact, postConfig).then(function(){
                initNewContact();
                vm.showNewContact = false;
                getContacts();
            }, function(error){
                console.log(error);
            });
        }

        function deleteContact(contact) {
            $http.delete(apiConstants.host+apiConstants.urls.deleteContact+'/'+contact._id).then(function() {
                getContacts();
            }, function (error) {
                console.log(error);
            });
        }

        function getContacts() {
            $http.post(apiConstants.host+apiConstants.urls.getContacts, vm.filterObject, postConfig).then(function(response) {
                vm.contactsViewModel = response.data;
                setMaxPageOptions();
            }, function (error) {
                console.log(error);
            });
        }

        function setMaxPageOptions() {
            var totalContacts = vm.contactsViewModel.totalContacts;
            var maxPage = Math.ceil(totalContacts / vm.filterObject.pageSize);
            vm.pageNumberOptions = [];
            for (var i = 1; i <= maxPage; i++) {
                vm.pageNumberOptions.push(i);
            }
            if (vm.filterObject.pageNumber > maxPage) {
                vm.filterObject.pageNumber = maxPage;
                getContacts();
            }
        }

        initNewContact();
        getContacts();
    }
}());