(function () {
    angular.module("app").config([
        "$routeProvider",
        function ($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: "views/contacts.html",
                    controller: "contactsController as cc"
                });
        }
    ]);
})();