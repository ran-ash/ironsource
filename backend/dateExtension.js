module.exports = {
    nowFormatted: nowFormatted
};

function nowFormatted() {
    return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}