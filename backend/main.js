const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-body');
const app = new Koa();
const router = new Router();
const cors = require('koa-cors');
const contactsService = require('./contactsService');
const dateExtension = require('./dateExtension');

app.use(cors());
app.use(bodyParser());

router.post('/api/getContacts', getContacts);
router.post('/api/addContact', addContact);
router.delete('/api/deleteContact/:id', deleteContact);

async function getContacts (context) {
    console.log('Enter getContacts');

    try {
        var filterObject = context.request.body;
        var contacts = await contactsService.getContacts(filterObject);
        context.body = contacts;
    } catch (error) {
        context.status = 500;
        context.body = error;
    }
    
    console.log('Leave getContacts');
}

async function addContact(context) {
    console.log('Enter addContact');

    try {
        var contact = context.request.body;
        await contactsService.insertContact(contact);
        context.body = { message: "Added " + contact._id} ;
    } catch (error) {
        context.status = 500;
        context.body = error;
    }

    console.log('Leave addContact');
}

async function deleteContact(context) {
    console.log('Enter addContact');

    try {
        var contactId = context.params.id;
        await contactsService.deleteContact(contactId);
        context.body = { message: "Deleted " + contactId } ;
    } catch (error) {
        context.status = 500;
        context.body = error;
    }
    console.log('Leave addContact');
}

app.use(async (context, next) => {
    console.log("A new request received at " + dateExtension.nowFormatted());
    await next();
    console.log("Request ended at " + dateExtension.nowFormatted());
});

app.use(router.routes());

app.listen(3000, () => {
    console.log(dateExtension.nowFormatted() + '  Listening on 3000');
});