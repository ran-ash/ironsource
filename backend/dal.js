module.exports = {
    insertContact: insertContact,
    deleteContact: deleteContact,
    getContacts: getContacts
};

var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
var url = "mongodb://localhost:27017/phonebookDb";
var collectionName = "contacts";
/// contact object structure
/*
{
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    _id: ''
}
*/
///

/// filter object structure
/*
{
    searchString: '',
    sortColumn: '',
    sortAscending: true/false,
    pageNumber: 1,
    pageSize: 100
}
*/
///

async function insertContact(contact) {
    contact.lowerFirst = contact.firstName ? contact.firstName.toLowerCase() : null;
    contact.lowerLast = contact.lastName ? contact.lastName.toLowerCase() : null;
    contact.lowerEmail = contact.email ? contact.email.toLowerCase() : null;
    contact.lowerAddress = contact.address ? contact.address.toLowerCase() : null;

    var db = await mongoClient.connect(url);
    var insertedContact = await db.collection(collectionName).insertOne(contact);
    contact._id = insertedContact.insertedId;
    console.log("inserted contact id: " + contact._id);
    db.close();
}

async function deleteContact(contactId) {
    var db = await mongoClient.connect(url);
    var query = {_id: new mongodb.ObjectID(contactId)};
    var deleteResponse = await db.collection(collectionName).deleteOne(query);
    if (deleteResponse.deletedCount == 1){
        console.log("deleted contact id: " + contactId);
    } else {
        console.log("contact id: " + contactId + " did not exist");
    }
    db.close();
}

async function getContacts(filterObject) {
    var contactsViewModel = {};
    var count = await getContactsCount(filterObject);
    var contacts = await getActualContacts(filterObject);

    return {
        totalContacts: count,
        contacts: contacts
    };
}


async function getContactsCount(filterObject) {
    var queryObject = getQueryObject(filterObject);
    var db = await mongoClient.connect(url);
    var contactsCount = await db.collection(collectionName).find(queryObject).count();
    console.log("total " + contactsCount + " contacts");
    db.close();
    return contactsCount;
}

async function getActualContacts(filterObject) {
    var queryObject = getQueryObject(filterObject);
    var fields = { lowerFirst: false, lowerLast: false, lowerEmail: false, lowerAddress: false };
    var sortObject = getSortObject(filterObject);
    var skip = (filterObject.pageNumber-1)*filterObject.pageSize;
    var limit = parseInt(filterObject.pageSize);

    var db = await mongoClient.connect(url);
    var contacts = await db.collection(collectionName).find(queryObject, fields).sort(sortObject).skip(skip).limit(limit).toArray();
    console.log("returning " + contacts.length + " contacts");
    db.close();
    return contacts;
}

function getSortObject(filterObject) {
    if (filterObject.sortColumn == undefined) {
        filterObject.sortColumn = "lowerFirst";
    }

    var ascending = filterObject.sortAscending ? 1 : -1;
    
    var matchingLowerField = "";
    switch (filterObject.sortColumn) {
        case "firstName":
            matchingLowerField = "lowerFirst";
            break;
        case "lastName":
            matchingLowerField = "lowerLast";
            break;
        case "email":
            matchingLowerField = "lowerEmail";
            break;
        case "address":
            matchingLowerField = "lowerAddress";
            break;
        default:
            matchingLowerField = "phone";
            break;
    }

    var sortObject = {};
    sortObject[matchingLowerField] = ascending;
    return sortObject;
}

function getQueryObject(filterObject) {
    if (filterObject.searchString == undefined || filterObject.searchString == '') {
        return {};
    }

    var regex = {"$regex": filterObject.searchString, "$options": "i"}
    return {
        $or:[
            {firstName: regex},
            {lastName: regex},
            {phone: regex},
            {email: regex},
            {address: regex},
        ]
    }
}