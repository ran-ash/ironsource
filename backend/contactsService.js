module.exports = {
    insertContact: insertContact,
    deleteContact: deleteContact,
    getContacts: getContacts
};

const emailValidator = require("./emailValidator");
const dal = require('./dal');

async function insertContact(contact) {
    var validationErrors = validateContact(contact);
    if (validationErrors.length > 0) {
        throw validationErrors;
    }

    return await dal.insertContact(contact);
}

async function deleteContact(contactId) {
    return await dal.deleteContact(contactId);
}

async function getContacts(filterObject) {
    return await dal.getContacts(filterObject);
}

function validateContact(contact) {
    var errors = [];
    if (!contact.firstName || contact.firstName == ''){
        errors.push("contact missing firstName");
    }
    if (contact.email && !emailValidator.validate(contact.email)) {
        errors.push("contact email is invalid");
    }
    return errors;
}